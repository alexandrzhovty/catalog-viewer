//
//  SNAppDelegate.h
//  CatalogViewer
//
//  Created by Шурик on 09.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const SNAppLastUpdateKey;

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


// APNs
@property (nonatomic, strong, readonly) NSData *theDeviceToken;
@property (nonatomic, strong) NSString* uuid;

+ (SNAppDelegate *)sharedAppDelegate;
- (void)checkUniqueDeviceID;

@end
