//
//  SNStatusView.m
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 11/25/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import "SNStatusView.h"


IB_DESIGNABLE
@implementation SNStatusView

- (CGSize)intrinsicContentSize {
    
    CGFloat height =  [[UIApplication sharedApplication] delegate].window.safeAreaInsets.top;
    
    return CGSizeMake(self.frame.size.width, MAX(height, 20));
}

@end
