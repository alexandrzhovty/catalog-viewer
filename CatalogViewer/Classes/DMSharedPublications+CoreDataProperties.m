//
//  DMSharedPublications+CoreDataProperties.m
//  
//
//  Created by Denis Harckevich on 04.07.17.
//
//

#import "DMSharedPublications+CoreDataProperties.h"

@implementation DMSharedPublications (CoreDataProperties)

+ (NSFetchRequest<DMSharedPublications *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DMSharedPublications"];
}

@dynamic oID;
@dynamic publications;

@end
