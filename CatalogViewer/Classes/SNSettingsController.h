//
//  SNSettingsController.h
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNSettingsControllerDeleate;

@interface SNSettingsController : UIViewController

@property (weak, nonatomic) id<SNSettingsControllerDeleate> delegate;

@end


@protocol SNSettingsControllerDeleate <NSObject>

- (void)settingsControllerDidSave:(SNSettingsController *)settingsController;

@end