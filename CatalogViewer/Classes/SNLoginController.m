//
//  SNLoginController.m
//  CatalogViewer
//
//  Created by Шурик on 09.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNLoginController.h"

#import "SNKeychainWrapper.h"


@interface SNLoginController ()
@property (nonatomic, strong) IBOutlet UITextField *userNameField;
@property (nonatomic, strong) IBOutlet UITextField *passwordField;
@end

@implementation SNLoginController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userNameField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    self.passwordField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecValueData];
}
//
//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 552, 384);
//}

- (void)dealloc
{
    NSLog(@"%s", __FUNCTION__);
}

#pragma mark - Status Bar
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonEnter:(id)sender
{
    // Check user name
    NSString *userName = [self.userNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableCharacterSet *allowedCharactes = [NSMutableCharacterSet characterSetWithCharactersInString:@"@._-"];
    [allowedCharactes formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
    [allowedCharactes formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    NSCharacterSet *disallowedCharacters = [allowedCharactes invertedSet];
    //    NSLog(@"%lu", (unsigned long)[userName rangeOfCharacterFromSet:disallowedCharacters].location);
    // Check password
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([userName rangeOfCharacterFromSet:disallowedCharacters].location != NSNotFound || [userName length] < 1 || [password length] < 1) {
        NSString *message = NSLocalizedString(@"User name or password you have entered is invalid. Please try again.", nil);
        UIAlertController *vc = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        [vc addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:vc animated:YES completion:NULL];
        return;
    }
    
    
    [[SNKeychainWrapper sharedWrapper] setObject:@"Myappstring" forKey:(__bridge id)kSecAttrService];
    [[SNKeychainWrapper sharedWrapper] setObject:userName forKey:(__bridge id)kSecAttrAccount];
    [[SNKeychainWrapper sharedWrapper] setObject:password forKey:(__bridge id)kSecValueData];
    
    [self.delegate loginController:self didEnterWithParameters:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
