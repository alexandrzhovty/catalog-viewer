//
//  SNLoginController.h
//  CatalogViewer
//
//  Created by Шурик on 09.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNLoginControllerDelegate;

@interface SNLoginController : UIViewController

@property (weak, nonatomic) id<SNLoginControllerDelegate> delegate;

@end

@protocol SNLoginControllerDelegate <NSObject>

- (void)loginController:(SNLoginController *)loginController didEnterWithParameters:(NSDictionary *)parametres;

@end
