//
//  SNBadgedButton.h
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 11/25/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface SNBadgedButton : UIView

@property (nonatomic) IBInspectable NSInteger count;

@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIImageView *badgeImageView;

@end

NS_ASSUME_NONNULL_END
