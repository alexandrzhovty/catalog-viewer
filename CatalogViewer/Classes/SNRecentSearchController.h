//
//  SNRecentSearchController.h
//  CatalogViewer
//
//  Created by Denis on 15.04.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *RecentSearchesKey;

@class SNRecentSearchController;

@protocol SNRecentSearchDelegate
// Sent when the user selects a row in the recent searches list.
- (void)recentSearchesController:(SNRecentSearchController *)controller didSelectString:(NSString *)searchString;
@end

@interface SNRecentSearchController : UITableViewController <UIActionSheetDelegate>

@property (nonatomic, assign) id <SNRecentSearchDelegate> delegate;
@property (nonatomic, retain) NSArray *recentSearches;
@property (nonatomic, retain) NSArray *displayedRecentSearches;

@property (nonatomic, retain) UIActionSheet *confirmSheet;

@property (nonatomic, retain) UIBarButtonItem *clearButtonItem;

- (void)addToRecentSearches:(NSString *)searchString;
- (void)filterResultsUsingString:(NSString *)filterString;

@end