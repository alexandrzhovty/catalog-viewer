//
//  Toolbox.h
//  CatalogService
//
//  Created by is Industrie Software GmbH on 15.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Toolbox : NSObject

- (UIImage *)convertImageToGrayScale:(UIImage *)image;
+ (float)getFreeSpace;
+ (NSString *)humanReadableFileSizeFromBytes:(unsigned long long)bytes;
+ (NSString *)documentsDirectoryPath;
+ (NSString *)getUniqueDeviceIdentifier;

@end
