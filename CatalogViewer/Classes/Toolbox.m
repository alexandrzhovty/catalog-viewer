//
//  Toolbox.m
//  CatalogService
//
//  Created by is Industrie Software GmbH on 15.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//

#import "Toolbox.h"

@implementation Toolbox

+ (NSString*)getUniqueDeviceIdentifier {
    // Create universally unique identifier (object)
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    
    // Get the string representation of CFUUID object.
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
    CFRelease(uuidObject);
    
    return uuidStr;
}


+ (NSString*)humanReadableFileSizeFromBytes:(unsigned long long)bytes {
    NSString* sizeLoaded = @"";
    unsigned long long tmpSize = 0;
    
    if ((bytes > 0) & (bytes < 1024)) {
        // Bytes
        sizeLoaded = [NSString stringWithFormat:@"%llu Byte", bytes];
    } else if ((bytes >= 1024) & (bytes < 1048576)) {
        // Kilobyte
        tmpSize = (bytes / (1024));
        sizeLoaded = [NSString stringWithFormat:@"%llu Kilobyte", tmpSize];
    } else if ((bytes >= (1048576)) & (bytes < 1048576*1024)) {
        // Megabyte
        tmpSize = (bytes / (1048576));
        sizeLoaded = [NSString stringWithFormat:@"%llu Megabyte", tmpSize];
    } else {
        // Gigabyte
        tmpSize = (bytes / (1048576*1024));
        sizeLoaded = [NSString stringWithFormat:@"%llu Gigabyte", tmpSize];
    }
    
    return sizeLoaded;
    
}


+ (float)getFreeSpace {
    float freeSpace = 0.0f;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemFreeSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];
        freeSpace = [fileSystemFreeSizeInBytes floatValue];
    } else {
        //Handle error
    }  
    return freeSpace; }

+ (NSString *)documentsDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    return documentsDirectoryPath;
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    //NSLog(@"convertImageToGrayScale: image.size.width: %f image.size.height: %f", image.size.width, image.size.height);
    if (image.size.width > 0) {
        
    
        // Grayscale color space
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
        // Create bitmap content with current image size and grayscale colorspace
        CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
        // Draw image into current context, with specified rectangle
        // using previously defined context (with grayscale colorspace)
        CGContextDrawImage(context, imageRect, [image CGImage]);
    
        // Create bitmap image info from pixel data in current context
        CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
        // Create a new UIImage object  
        UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
        // Release colorspace, context and bitmap information
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        CFRelease(imageRef);
        return newImage;
    } else {
        return image;
    }
    
    // Return the new grayscale image
    
}

@end
