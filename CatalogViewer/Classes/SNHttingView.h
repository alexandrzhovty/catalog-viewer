//
//  SNHttingView.h
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 12/5/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol SNHttingViewDelegate;

@interface SNHttingView : UIView
@property (weak, nonatomic) id<SNHttingViewDelegate> delegate;
@end

@protocol SNHttingViewDelegate <NSObject>

- (void)hittingView:(SNHttingView *)hittingView shouldHideMenu:(BOOL)hidden;

@end

NS_ASSUME_NONNULL_END
