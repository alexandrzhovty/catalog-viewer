//
//  FirstViewController.h
//  CatalogService
//
//  Created by is Industrie Software GmbH on 02.11.11.
//  Copyright 2011 is Industrie Software GmbH. All rights reserved.
//


#import "SNPublicationsController.h"

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const SNAppShowNewPublicationKey;

@interface SNPublicationsController : UIViewController
{
    __weak IBOutlet UISearchBar         *_searchBar;
}

@end

