//
//  SNCategoriesController.m
//  CatalogViewer
//
//  Created by Шурик on 13.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNCategoriesController.h"

#import "DMManager.h"

@interface SNCategoriesController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak,  nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SNCategoriesController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.preferredContentSize = CGSizeMake(300.0, 700.0);
    [self selectCategory:nil];
}

#pragma mark - Utilities
- (void)selectCategory:(DMCategory *)category
{
    if (category != nil) {
        if (category.parent != nil)  return;
        
        {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DMCategory"];
            request.predicate = [NSPredicate predicateWithFormat:@"parent == %@", category];
            NSError *error = nil;
            NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
            if (!results) {
                NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
                abort();
            } else {
                for (DMCategory* val in results) {
                    val.isShown = @(YES);
                }
            }
        }
        {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DMCategory"];
            request.predicate = [NSPredicate predicateWithFormat:@"parent != %@ AND parent != nil", category];
            NSError *error = nil;
            NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
            if (!results) {
                NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
                abort();
            } else {
                for (DMCategory* val in results) {
                    val.isShown = @(NO);
                }
            }
        }
    } else {
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMCategory class])];
//        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"parent != nil"];
        
        // Update new fields
        NSBatchUpdateRequest *batchRequest = [NSBatchUpdateRequest batchUpdateRequestWithEntityName:NSStringFromClass([DMCategory class])];
        batchRequest.predicate =[NSPredicate predicateWithFormat:@"parent != nil"];
        batchRequest.propertiesToUpdate = @{ @"isShown" : @(NO) };
        
        batchRequest.resultType = NSUpdatedObjectsCountResultType;
        NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
        NSError *error = nil;
        [context executeRequest:batchRequest error:&error];
        if (error) {
            NSLog(@"%s Error during fetching butch update: %@", __FUNCTION__, error);
        }
    }
}

#pragma mark - Table View Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMCategory *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (category.parent) {
        cell.textLabel.text = [@"- " stringByAppendingString:category.title];
    } else {
        cell.textLabel.text = category.title;
    }
    
    UIImageView *imageView = nil;
    if (self.currentCategory == category) {
//        UIImage *image = [UIImage imageNamed:@"categoryAccessory"];
//        imageView = [[UIImageView alloc] initWithImage:image];
        cell.textLabel.textColor = [UIColor colorWithRed:83/255.f green:154/255.f blue:60/255.f alpha:1];
//        imageView.backgroundColor = [UIColor blackColor];
//        cell.backgroundColor = [UIColor blackColor];
    } else {
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    cell.accessoryView = imageView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DMCategory *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (_currentCategory != nil &&  category != _currentCategory) {
        NSIndexPath* indexPath = [self.fetchedResultsController indexPathForObject:_currentCategory];
        _currentCategory = nil;
        
        [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
    }
    
    _currentCategory = category;
    [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
    [self selectCategory:category];
    [self.delegate categoriesController:self didSelectCategory:category];
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMCategory *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSInteger idx = category.parent == nil ? 0 : 1;
    return idx;
}

#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMCategory class])];
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDesc1 = [[NSSortDescriptor alloc] initWithKey:DMCategoryCompareFieldKey ascending:YES];
    fetchRequest.sortDescriptors = @[sortDesc1];
    
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isShown = %@", @(YES)];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
