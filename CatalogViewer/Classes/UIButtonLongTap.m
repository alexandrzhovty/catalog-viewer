//
//  UIButtonLongTap.m
//  CatalogService
//
//  Created by is Industrie Software GmbH on 15.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//

#import "UIButtonLongTap.h"
#import <QuartzCore/QuartzCore.h>


@implementation UIButtonLongTap
@synthesize delegate;
@synthesize showDeleteBadge;
@synthesize isNew;
@synthesize isLocal;

- (void)detectedLongTap {
    consumedTap = YES;
    NSLog(@"detectedLongTap");
    if (delegate != nil && [delegate respondsToSelector:@selector(buttonDidLongTap:)]) {
        [delegate performSelector:@selector(buttonDidLongTap:) withObject:[NSNumber numberWithInt:self.tag]];
        [self showDeleteState:YES];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesBegan");
    if (consumedTap ==  YES) {
        consumedTap = NO;
        [self showDeleteState:NO];
    }
    [super touchesBegan:touches withEvent:event];
    [self performSelector:@selector(detectedLongTap) withObject:nil afterDelay:1.0];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!consumedTap) {
        NSLog(@"touchesEnded");
        [super touchesEnded:touches withEvent:event];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(detectedLongTap) object:nil];
    }
}

- (void)addShadow {
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowRadius = 4;
    self.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    self.layer.cornerRadius=4.0f;
    self.layer.masksToBounds = NO;
}

/*
 *  Action is called if User Taps on Delete Icon in upper right Corner
 *  calls Delegate -> deleteSignalForTag: if defined in Delegate
 */
- (void)deleteTapped:(id)sender {
    if (delegate != nil && [delegate respondsToSelector:@selector(deleteSignalForTag:)]) {
        [delegate performSelector:@selector(deleteSignalForTag:) withObject:[NSNumber numberWithInt:self.tag]];
        [self showDeleteState:NO];
        consumedTap = NO;
    }
}

/*
 *  Method displays a red cross in the upper right corner of Button
 *  @param state BOOL   YES -> Display Badge    NO -> Don't display Badge
 *
 *  If the NEWState Badge is displayed it will be removed to display the Remove-Badge!
 */
- (void)showDeleteState:(BOOL)state {
    [self showNewState:NO];
    if (state == YES) {
        if (deleteButton == nil) {
            deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *btnImage = [[UIImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/delete.png", [[NSBundle mainBundle] bundlePath]]];
            [deleteButton setBackgroundImage:btnImage  forState:UIControlStateNormal];
            deleteButton.frame = CGRectMake(85, 0, 32, 32);
            [deleteButton addTarget:self action:@selector(deleteTapped:) forControlEvents:UIControlEventTouchDown];
            [self addSubview:deleteButton];
            [self setHighlighted:NO];
        }
    } else {
        if (isNew == YES) {
            [self showNewState:YES];
        }
        if (deleteButton != nil) {
            deleteButton.hidden = YES;
            [self bringSubviewToFront:deleteButton];
            [deleteButton removeFromSuperview];
            deleteButton = nil;
        }
    }
}

/*
 *  Method hooks a "NEW" Badge in the upper right corner of Button
 *  @param state BOOL   YES -> Display Badge    NO -> Don't display Badge
 */

- (void)showNewState:(BOOL)state {
    if (state == YES) {
        isNew = YES;
        if (newBadgeView == nil) {
            newBadgeView = [[UIImageView alloc] initWithFrame:CGRectMake(42, 54, 51, 74)];
            UIImage *btnImage = [[UIImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/new_batch.png", [[NSBundle mainBundle] bundlePath]]];
            newBadgeView.image = btnImage;
            newBadgeView.layer.shadowColor = [UIColor blackColor].CGColor;
            newBadgeView.layer.shadowOpacity = 0.5;
            newBadgeView.layer.shadowRadius = 4;
            newBadgeView.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
            newBadgeView.layer.cornerRadius=4.0f;
            newBadgeView.layer.masksToBounds = NO;
            [self addSubview:newBadgeView];
            [self setAlpha:0.4];
        }
    } else {
        if (newBadgeView != nil) {
            newBadgeView.hidden = YES;
            [self bringSubviewToFront:newBadgeView];
            [newBadgeView removeFromSuperview];
            newBadgeView = nil;
            [self setAlpha:1.0];
        }
    }
}

- (void)dealloc {
    newBadgeView = nil;
    deleteButton = nil;
    
}

@end
