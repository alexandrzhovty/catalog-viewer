//
//  DMSharedPublications+CoreDataProperties.h
//  
//
//  Created by Denis Harckevich on 04.07.17.
//
//

#import "DMSharedPublications+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DMSharedPublications (CoreDataProperties)

+ (NSFetchRequest<DMSharedPublications *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *oID;
@property (nullable, nonatomic, retain) NSSet<DMPublication *> *publications;

@end

@interface DMSharedPublications (CoreDataGeneratedAccessors)

- (void)addPublicationsObject:(DMPublication *)value;
- (void)removePublicationsObject:(DMPublication *)value;
- (void)addPublications:(NSSet<DMPublication *> *)values;
- (void)removePublications:(NSSet<DMPublication *> *)values;

@end

NS_ASSUME_NONNULL_END
