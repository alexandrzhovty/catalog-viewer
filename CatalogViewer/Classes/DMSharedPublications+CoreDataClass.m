//
//  DMSharedPublications+CoreDataClass.m
//  
//
//  Created by Denis Harckevich on 04.07.17.
//
//

#import "DMSharedPublications+CoreDataClass.h"
#import "DMPublication+Category.h"
#import "DMManager.h"

@implementation DMSharedPublications

+ (nonnull DMSharedPublications *)sharedInContext:(nullable NSManagedObjectContext*)context
{
    NSNumber *defaultID = @(0);
    NSManagedObjectContext *defaultContext = [[DMManager sharedManager] defaultContext];
    
    // check whether there is already created object
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSharedPublications class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"oID = %@", defaultID];
    fetchRequest.fetchLimit = 1;
    NSArray *results = [defaultContext executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        return [results lastObject];
    }
    
    // create default object
    DMSharedPublications *shared = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMSharedPublications class]) inManagedObjectContext:defaultContext];
    shared.oID = defaultID;
    
    // save it
    NSError *error = nil;
    if (![defaultContext save:&error]) {
        NSLog(@"%s error: %@", __FUNCTION__, error);
    }
    
    // check context
    if (context != nil) {
        if (context != defaultContext) {
            shared = (DMSharedPublications *)[context objectWithID:shared.objectID];
        }
    }
    
    return shared;
}

@end
