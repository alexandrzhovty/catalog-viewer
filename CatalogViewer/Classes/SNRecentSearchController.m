//
//  SNRecentSearchController.m
//  CatalogViewer
//
//  Created by Denis on 15.04.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNRecentSearchController.h"

NSString *RecentSearchesKey = @"RecentSearchesKey";

@interface SNRecentSearchController ()

@end

@implementation SNRecentSearchController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Last Search", nil);
    self.contentSizeForViewInPopover = CGSizeMake(300.0, 280.0);
    
    // Set up the recent searches list, from user defaults or using an empty array.
    NSArray *recents = [[NSUserDefaults standardUserDefaults] objectForKey:RecentSearchesKey];
    if (recents) {
        self.recentSearches = recents;
        self.displayedRecentSearches = recents;
    }
    else {
        self.recentSearches = [NSArray array];
        self.displayedRecentSearches = [NSArray array];
    }
    
    // Create a button item to clear the recents list.
    UIButton *clearButton = [[UIButton alloc] init];
    [clearButton setTitle:NSLocalizedString(@"Clear", nil) forState:UIControlStateNormal];
    [clearButton setFrame:CGRectMake(0, 0, 60, 30)];
    [clearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [clearButton addTarget:self action:@selector(showClearRecentsAlert:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *aButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showClearRecentsAlert:)];
    UIBarButtonItem *aButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearButton];
    self.clearButtonItem = aButtonItem;
    
    if ([_recentSearches count] == 0) {
        // Disable the button if there are no recents items.
        _clearButtonItem.enabled = NO;
    }
    self.navigationItem.leftBarButtonItem = _clearButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    // Ensure the complete list of recents is shown on first display.
    [super viewWillAppear:animated];
    self.displayedRecentSearches = _recentSearches;
}

- (void)viewDidUnload
{    [super viewDidUnload];
    self.recentSearches = nil;
    self.displayedRecentSearches = nil;
}

#pragma mark - Managing the recents list
- (void)addToRecentSearches:(NSString *)searchString
{
    // Filter out any strings that shouldn't be in the recents list.
    if ([searchString isEqualToString:@""]) {
        return;
    }
    
    // Create a mutable copy of recent searches and remove the search string if it's already there (it's added to the top of the list later).
    
    NSMutableArray *mutableRecents = [_recentSearches mutableCopy];
    [mutableRecents removeObject:searchString];
    
    // Add the new string at the top of the list.
    [mutableRecents insertObject:searchString atIndex:0];
    
    // Update user defaults.
    [[NSUserDefaults standardUserDefaults] setObject:mutableRecents forKey:RecentSearchesKey];
    
    // Set self's recent searches to the new recents array, and reload the table view.
    self.recentSearches = mutableRecents;
    self.displayedRecentSearches = mutableRecents;
    [self.tableView reloadData];
    
    // Ensure the clear button is enabled.
    _clearButtonItem.enabled = YES;
}


- (void)filterResultsUsingString:(NSString *)filterString
{
    // If the search string is zero-length, then restore the recent searches,
    // otherwise create a predicate to filter the recent searches using the search string.
    //
    if ([filterString length] == 0) {
        self.displayedRecentSearches = _recentSearches;
    }
    else {
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", filterString];
        NSArray *filteredRecentSearches = [_recentSearches filteredArrayUsingPredicate:filterPredicate];
        self.displayedRecentSearches = filteredRecentSearches;
    }
    
    [self.tableView reloadData];
}


- (void)showClearRecentsAlert:(id)sender
{
    // If the user taps the Clear Recents button, present an action sheet to confirm.
    _confirmSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Clear All Recents", nil) otherButtonTitles:nil];
    [_confirmSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        // If the user chose to clear recents, remove the recents entry from user defaults,
        // set the list to an empty array, and redisplay the table view.
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:RecentSearchesKey];
        self.recentSearches = [NSArray array];
        self.displayedRecentSearches = [NSArray array];
        [self.tableView reloadData];
        _clearButtonItem.enabled = NO;
    }
    self.confirmSheet = nil;
}


#pragma mark Table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return [_displayedRecentSearches count];
}


// Display the strings in displayedRecentSearches.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [_displayedRecentSearches objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Notify the delegate if a row is selected.
    [_delegate recentSearchesController:self didSelectString:[_displayedRecentSearches objectAtIndex:indexPath.row]];
}

@end
