//
//  SNHttingView.m
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 12/5/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import "SNHttingView.h"

@implementation SNHttingView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
//    if self.point(inside: point, with: event) {
//        let convertedPoint = followButton.convert(point, from: self)
//        if let hitTestView = followButton.hitTest(convertedPoint, with: event) {
//            return hitTestView
//        }
//
//        let convertedPointVB = verifiedButton.convert(point, from: self)
//        if let hitTestView = verifiedButton.hitTest(convertedPointVB, with: event) {
//            return hitTestView
//        }
//
//
//    }
    if ([self pointInside:point withEvent:event] == YES) {
        for (UIView *view in self.subviews) {
            CGPoint convertedPoint = [view convertPoint:point fromView:self];
            UIView *hitTestView = [view hitTest:convertedPoint withEvent:event];
            if( hitTestView != nil) return hitTestView;
        }
    }
    [self.delegate hittingView:self shouldHideMenu:YES];
    return nil;
}

@end
