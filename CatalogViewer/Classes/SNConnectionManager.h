//
//  SNConnectionManager.h
//


#import <Foundation/Foundation.h>


FOUNDATION_EXTERN NSString *const SNConnectionExpectedContentLengthKey;
FOUNDATION_EXTERN NSString *const SNConnectionReceivedContentLengthKey;
FOUNDATION_EXTERN NSString *const SNConnectionDidReceiveDataEvent;
FOUNDATION_EXTERN NSString *const SNConnectionAPIServerURIValue;
FOUNDATION_EXTERN NSString *const SNConnectionAPIAppKey;
FOUNDATION_EXTERN NSString *const SNConnectionProjectImageURIValue;
FOUNDATION_EXTERN NSString *const SNConnectionCustomerImageURIValue;


//typedef void (^ConnectionCompletionHandler)(id recievedData, NSURLResponse *response, NSError *error);

typedef NS_ENUM(NSUInteger, SNURLReqeustType)
{
    SNURLReqeustTypeGet,
    SNURLReqeustTypePost,
    SNURLReqeustTypeDelete
};

@interface SNConnectionManager : NSObject


+ (void)stopAllConnections;
+ (void)stopConnection:(NSURLConnection *)connection;
+ (NSURLConnection *)performRequest:(NSURLRequest *)request completionHandler:(void (^)(id recievedData, NSURLResponse *response, NSError *error))completionHandler;
+ (SNConnectionManager *)sharedManager;
+ (NSURLRequest *)requestMethodName:(NSString *)methodName paramaters:(NSDictionary *)parameters requestType:(SNURLReqeustType)requestType;

@end

