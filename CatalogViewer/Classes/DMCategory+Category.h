//
//  DMCategory+Category.h
//  TestRestKit
//
//  Created by Шурик on 19.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMCategory.h"

FOUNDATION_EXTERN NSString * const DMCategoryIDKey;
FOUNDATION_EXTERN NSString * const DMCategoryParentKey;
FOUNDATION_EXTERN NSString * const DMCategoryTitleKey;
FOUNDATION_EXTERN NSString * const DMCategoryCompareFieldKey;

@interface DMCategory (Category)

+ (DMCategory *)categoryByID:(NSNumber *)categoryID inContext:(NSManagedObjectContext *)context;

+ (NSURLRequest *)requestCategories;
- (BOOL)isFavoriteCategory;

+ (void)parseResponse:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context;

+ (DMCategory *)commonCategoryInContext:(NSManagedObjectContext *)context;
+ (DMCategory *)favoritesCategoryInContext:(NSManagedObjectContext *)context;

@end
