//
//  pageFlipViewController.h
//  CatalogService
//
//  Created by is Industrie Software GmbH on 03.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface pageFlipViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate>{
    
    UIWebView* theWebView;
    id delegate;
    NSString* theStartURL;
    NSInteger* thePubID;
    NSInteger downloadAllowed;
    MBProgressHUD* theHUD;
    UIBarButtonItem* bbItemSendLink;
    UIButton* triangleButton;
    
}



@property (nonatomic, strong) IBOutlet UIWebView* theWebView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* activityIndicator;
@property (nonatomic, strong) IBOutlet UILabel* thePubTitle;
@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) NSString* theStartURL;
@property (nonatomic, assign) NSInteger* thePubID;
@property (nonatomic, assign) NSInteger  downloadAllowed;
@property (nonatomic, strong) IBOutlet UINavigationBar* theNavigationBar;
@property (nonatomic, strong) IBOutlet UIToolbar* theToolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* cameraButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* bbItemSendLink;
@property (nonatomic, strong) IBOutlet UIButton* triangleButton;

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)req navigationType:(UIWebViewNavigationType)navigationType;
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webViewDidStartLoad:(UIWebView *)webView;
- (IBAction)backToHomeTapped:(id)sender;
- (IBAction)cameraIconTapped:(id)sender;
- (IBAction)sendPublicationTapped:(id)sender;
- (IBAction)toggleNavigationBar:(id)sender;

@end

@protocol pageFlipDelegate <NSObject>
// recipe == nil on cancel
- (void)didAskForClose:(id)sender;

@end