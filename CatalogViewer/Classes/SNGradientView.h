//
//  SNGradientView.h
//  CatalogViewer
//
//  Created by Шурик on 10.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNGradientView : UIView

// Storyboard user defined attributes
@property (strong, nonatomic) UIColor *startColor;
@property (strong, nonatomic) UIColor *endColor;

@end
