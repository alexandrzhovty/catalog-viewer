//
//  DMPublication+Category.h
//  CatalogViewer
//
//  Created by Шурик on 20.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMPublication.h"

FOUNDATION_EXTERN NSString * const DMPublicationIDKey;
FOUNDATION_EXTERN NSString * const DMPublicationCrearedKey;
FOUNDATION_EXTERN NSString * const DMPublicationCategoriesKey;
FOUNDATION_EXTERN NSString * const DMPublicationTitleKey;
FOUNDATION_EXTERN NSString * const DMPublicationStoredKey;

@interface DMPublication (Category)

+ (DMPublication *)publicationById:(NSNumber *)pid inContext:(NSManagedObjectContext *)context;
+ (NSArray *)sortDescriptors;
+ (NSURLRequest *)requestPublications;
+ (NSURLRequest *)requestRelations;

+ (void)parseResponse:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context;
+ (void)parseRelations:(NSDictionary *)dictoanry inContext:(NSManagedObjectContext *)context;


- (NSString *)storePath;
- (NSString *)thumbnailPath;




@end
