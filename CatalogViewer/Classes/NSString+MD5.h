//
//  NSString+MD5.h
//  CatalogService
//
//  Created by is Industrie Software GmbH on 28.02.12.
//  Copyright (c) 2012 is Industrie Software GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString*)md5;
@end

