//
//  SNToolbar.h
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 11/25/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface SNToolbar : UIView

@end

NS_ASSUME_NONNULL_END
