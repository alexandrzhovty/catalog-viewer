//
//  SNGradientView.m
//  CatalogViewer
//
//  Created by Шурик on 10.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNGradientView.h"

@interface SNGradientView ()
{
    CGGradientRef _gradientRef;
}

@end

@implementation SNGradientView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    const CGFloat *startComponets = CGColorGetComponents(_startColor.CGColor);
    CGFloat redStart     = startComponets[0];
    CGFloat greenStart = startComponets[1];
    CGFloat blueStart   = startComponets[2];
    CGFloat alphaStart = startComponets[3];
    
    const CGFloat *endComponents = CGColorGetComponents(_endColor.CGColor);
    CGFloat redEnd     = endComponents[0];
    CGFloat greenEnd = endComponents[1];
    CGFloat blueEnd   = endComponents[2];
    CGFloat alphaEnd = endComponents[3];
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGFloat colors[] =
    {
        redStart, greenStart, blueStart, alphaStart,
        redEnd, greenEnd, blueEnd, alphaEnd,
    };
    
    _gradientRef = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors) / (sizeof(colors[0]) * 4));
    CGColorSpaceRelease(rgb);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint start = rect.origin;
    start.y = 0;
    CGPoint end = CGPointMake(rect.origin.x, rect.size.height);
    CGContextDrawLinearGradient(context, _gradientRef, start, end, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    
    
}

@end
