//
//  SNToolbar.m
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 11/25/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import "SNToolbar.h"

@interface SNToolbar()
@property (strong, nonatomic) IBOutlet UIView *contentView;
@end

@implementation SNToolbar

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
//    [[NSBundle mainBundle] loadNibNamed:@"SNToolbar" owner:nil options:nil];
//    [self addSubview:self.contentView];
//    self.contentView.frame = self.bounds;

    UIView *view = [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"SNToolbar" owner:self options:nil] firstObject];
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:view];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
}

@end
