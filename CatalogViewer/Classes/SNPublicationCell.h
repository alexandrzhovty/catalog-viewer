//
//  SNPublicationCell.h
//  CatalogViewer
//
//  Created by Шурик on 20.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMPublication;

@interface SNPublicationCell : UICollectionViewCell


@property (strong, nonatomic) DMPublication *publication;

@end
