//
//  SNBadgedButton.m
//  CatalogViewer
//
//  Created by Aleksandr Zhovtyi on 11/25/18.
//  Copyright © 2018 Alexandr Zhovty. All rights reserved.
//

#import "SNBadgedButton.h"

@implementation SNBadgedButton


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    //    [[NSBundle mainBundle] loadNibNamed:@"SNToolbar" owner:nil options:nil];
    //    [self addSubview:self.contentView];
    //    self.contentView.frame = self.bounds;
    self.backgroundColor = [UIColor clearColor];
    UIView *view = [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"SNBadgedButton" owner:self options:nil] firstObject];
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:view];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
}

//- (CGSize)intrinsicContentSize {
//
//    CGFloat width =  23 + self.badgeImageView.frame.size.width;
//    return CGSizeMake(width, self.frame.size.height);
//}


- (void)setCount:(NSInteger)count {
    [self.countLabel setHidden: (count <= 0)];
    [self.badgeImageView setHidden:self.countLabel.isHidden];
    self.countLabel.text = [[NSNumber numberWithInteger:count] stringValue];
}

@end
