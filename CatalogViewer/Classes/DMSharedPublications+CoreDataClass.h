//
//  DMSharedPublications+CoreDataClass.h
//  
//
//  Created by Denis Harckevich on 04.07.17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMPublication;

NS_ASSUME_NONNULL_BEGIN

@interface DMSharedPublications : NSManagedObject

+ (nonnull DMSharedPublications *)sharedInContext:(nullable NSManagedObjectContext*)context;

@end

NS_ASSUME_NONNULL_END

#import "DMSharedPublications+CoreDataProperties.h"
