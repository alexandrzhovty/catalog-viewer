//
//  SNPublicationCell.m
//  CatalogViewer
//
//  Created by Шурик on 20.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNPublicationCell.h"

#import "DMManager.h"

@interface SNPublicationCell ()
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIImageView *_newImageView;
}

@end



@implementation SNPublicationCell

#pragma mark - Property Accessors
- (void)awakeFromNib {
    _titleLabel.highlightedTextColor = [UIColor colorWithRed:.6 green:.6 blue:.6 alpha:1];
}

- (void)setPublication:(DMPublication *)publication
{
    if (_publication != publication) {
        _publication = publication;
    }
    
    UIImage *image = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[publication thumbnailPath]]) {
        image = [[UIImage alloc] initWithContentsOfFile:[publication thumbnailPath]];
    } else {
        
    }
    
    _imageView.image = image;
    _imageView.alpha = [_publication.inLocalStore boolValue] ? 1.f : .8f;
    
    _newImageView.hidden = [_publication.inLocalStore boolValue];
    
    
    CGFloat width = CGRectGetWidth(self.frame) - CGRectGetMinX(_titleLabel.frame) * 2;
    
    UIFont *font = _titleLabel.font;
    CGSize size = CGSizeMake(width, font.lineHeight * 3 + 4);
    NSString *title = _publication.title;
    size = [title sizeWithFont:font constrainedToSize:size lineBreakMode:_titleLabel.lineBreakMode];
    
    NSMutableAttributedString *mutAttrStr = [[NSMutableAttributedString alloc] init];
    
    if ([publication.unread boolValue] == YES) {
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:@"Icon-New"];
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [mutAttrStr appendAttributedString:attrStringWithImage];
    }
    
    

    [mutAttrStr appendAttributedString:[[NSAttributedString alloc] initWithString:title attributes:@{ NSFontAttributeName: _titleLabel.font}]];
                                                                                                      
    
    
//    CGRect frame = _titleLabel.frame;
//    frame.size.height = size.height;
//    _titleLabel.frame = frame;
    _titleLabel.attributedText = mutAttrStr;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.frame) - CGRectGetMinX(_titleLabel.frame) * 2;
    
    UIFont *font = _titleLabel.font;
    CGSize size = CGSizeMake(width, font.lineHeight * 3 + 4);
    CGRect rect = [_titleLabel.attributedText boundingRectWithSize:size options:(NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin) context:NULL];
    
    CGFloat height = MAX(font.lineHeight * 3, CGRectGetHeight(rect));
    CGRect frame = _titleLabel.frame;
    frame.size.height = height;
    _titleLabel.frame = frame;
    
    
    
}

@end
