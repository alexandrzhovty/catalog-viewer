//
//  pageFlipViewController.m
//  CatalogService
//
//  Created by is Industrie Software GmbH on 03.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//

#import "pageFlipViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+MD5.h"

@implementation pageFlipViewController

@synthesize theWebView;
@synthesize activityIndicator;
@synthesize delegate;
@synthesize theStartURL;
@synthesize thePubID;
@synthesize thePubTitle;
@synthesize theNavigationBar;
@synthesize theToolbar;
@synthesize cameraButton;
@synthesize bbItemSendLink;
@synthesize downloadAllowed;
@synthesize triangleButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        // Set user agent (Needed for the UIWebView because the JavaScript handles the dispays different for different browsers)
        NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3", @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
        self.title = @"DEMO";
        
        [triangleButton setBackgroundImage:[UIImage imageNamed:@"button_bar_move.png"] forState:UIControlStateNormal];
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[[theNavigationBar.items objectAtIndex:0] setTitle:thePubTitle.text];
    //[[self navigationItem] setTitle:@"HALLO"];
    
    NSString* pubPathComponent = [NSString stringWithFormat:@"pub%i", (int)thePubID];
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"startURL: %@", theStartURL);
    NSLog(@"downloadAllowed %i", downloadAllowed);
#endif
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask ,YES );
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pubPathComponent];
    //path = [path stringByAppendingPathComponent:@"index.html"];
    path = [path stringByAppendingPathComponent:theStartURL];
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"thePath: %@", path);
#endif
    [theWebView setDelegate:self];
    [theWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRight.delegate = self;
    [theWebView addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeLeft.delegate = self;
    [theWebView addGestureRecognizer:swipeLeft];
    
    // Changed in Version 1.2.5
    //
    // UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleNavigationBar:)];
    // swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    // swipeDown.numberOfTouchesRequired = 2;
    // swipeDown.delegate = self;
    // [theWebView addGestureRecognizer:swipeDown];
    
    // Show Hide Controls
    // depending on publication-settings
    if (downloadAllowed == 1) {
        self.bbItemSendLink.enabled = YES;
    } else {
        self.bbItemSendLink.enabled = NO;
    }
}

// GESTURE RECOGNIZING FUNCTIONALITY
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)swipeRightAction:(id)ignored
{
    //NSLog(@"Swipe Right");
    //add Function
}

- (void)swipeLeftAction:(id)ignored
{
    //NSLog(@"Swipe Left");
    //add Function
}

- (IBAction)toggleNavigationBar:(id)sender {
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"GESTURE: hiding/showing navigation BAR");
#endif
    if ([theToolbar isHidden]) {
        NSLog(@"showing Navigation Bar");
        
        [UIToolbar beginAnimations:@"NavBarFadeIn" context:nil];
        theToolbar.alpha = 0;
        [theToolbar setHidden:NO];
        [UIToolbar setAnimationCurve:UIViewAnimationCurveEaseIn]; 
        [UIToolbar setAnimationDuration:1.0];
        theToolbar.alpha = 0.7;
        [UIToolbar commitAnimations];
    } else {
        NSLog(@"hiding Navigation Bar");
        [UIToolbar beginAnimations:@"NavBarFadeOut" context:nil];
        theToolbar.alpha = 0.7;
        [UIToolbar setAnimationCurve:UIViewAnimationCurveEaseOut]; 
        [UIToolbar setAnimationDuration:1.0];
        theToolbar.alpha = 0;
        [theToolbar setHidden:YES];
        [UIToolbar commitAnimations];
    }
}


- (IBAction)backToHomeTapped:(id)sender {
    NSURLRequest* stopRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"about:blank"]];
    [self.theWebView loadRequest:stopRequest];
    if([[self delegate] respondsToSelector:@selector(didAskForClose:)]) {
        [[self delegate] didAskForClose:nil];
    }
}


- (IBAction)cameraIconTapped:(id)sender {
    // Take a Screenshot
    NSLog(@"Taking screenshot");
    UIGraphicsBeginImageContext(self.view.bounds.size);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);

    
    
    // Send it via Mail
    // Email:
     NSLog(@"Composing Mail");
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"This is the subject"];
	
	NSArray *toRecipients = [NSArray arrayWithObject:@"email@email.com"]; 
	[picker setToRecipients:toRecipients];
	
	NSData *imageData = UIImageJPEGRepresentation(viewImage, 1);
	[picker addAttachmentData:imageData mimeType:@"image/jpg" fileName:@"Screenshot.jpg"];
	
	NSString *emailBody = @"Text on the body!";
	[picker setMessageBody:emailBody isHTML:YES];
	 NSLog(@"Presenting Mail...");
	[self presentModalViewController:picker animated:YES];
    
}


- (IBAction)sendPublicationTapped:(id)sender {
    
    // Send it via Mail
    // Email:
    NSLog(@"Composing URL - Mail");
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	//[picker setSubject:@" "];
	
	//NSArray *toRecipients = [NSArray arrayWithObject:@" "]; 
	//[picker setToRecipients:toRecipients];
		
	NSString *downLoadURL = [NSString stringWithFormat:@"%d", (int)thePubID];
    NSString *emailBody = [NSString stringWithFormat:@"Download-URL:\r\n \r\n http://download.catalog-viewer.de/dispatcher/?%@   \r\n \r\nIf the link does not work from your e-mail program, please copy it to the clipboard and paste it into the address bar of your preferred browser. \r\n \r\npowered by catalogviewer.de", [downLoadURL md5]];
	[picker setMessageBody:emailBody isHTML:NO];
    NSLog(@"Presenting Mail...");
	[self presentModalViewController:picker animated:YES];
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self dismissModalViewControllerAnimated:YES];
}    


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)req navigationType:(UIWebViewNavigationType)navigationType {
    NSMutableURLRequest *request = (NSMutableURLRequest *)req;
    
    if ([request respondsToSelector:@selector(setValue:forHTTPHeaderField:)]) {
        /*
         Changed in 1.2.3
         [request setValue:[NSString stringWithFormat:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3", [request valueForHTTPHeaderField:@"User-Agent"]] forHTTPHeaderField:@"User_Agent"];
         */
         [request setValue:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3" forHTTPHeaderField:@"User_Agent"];
    }

    return YES; 
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [activityIndicator stopAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [activityIndicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [activityIndicator startAnimating];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	// return (interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
    if ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)){
        // Device will switch to Portrait - Mode
        NSLog(@"shouldAutoRotateToInterfaceOrientation: Portrait");
        [theWebView stringByEvaluatingJavaScriptFromString:@"window.__defineGetter__('orientation', function(){return 0;});"];
        //[theWebView reload];
    } else if ((interfaceOrientation == UIInterfaceOrientationLandscapeRight) || (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        // Device will switch to Landscape - Mode
         NSLog(@"shouldAutoRotateToInterfaceOrientation: Landscape");
        [theWebView stringByEvaluatingJavaScriptFromString:@"window.__defineGetter__('orientation', function(){return 90;});"];
        //[theWebView reload];
    }
    return YES;
}

@end
