//
//  DMManager.m
//  MSDCatalog
//
//  Created by Александр Жовтый on 09.02.13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "DMManager.h"


NSString *const DMManagerModelNameValue       = @"CatalogViewer";
NSString *const DMManagerErrorDomain          = @"com.dbmanager";

@implementation DMManager
{
    BOOL _isLoading;
}

@synthesize defaultContext = _defaultContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




#pragma mark - Initialization

+ (DMManager *)sharedManager
{
    static dispatch_once_t once;
    static DMManager *__inctance;
    dispatch_once(&once, ^ { __inctance = [[DMManager alloc] init]; });
    return __inctance;
}

+ (NSManagedObjectContext *)managedObjectContext
{
    return [[DMManager sharedManager] defaultContext];
}

- (NSURL *)storeURL
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return [url URLByAppendingPathComponent:[DMManagerModelNameValue stringByAppendingString:@".sqlite"]];
}


#pragma mark - Core Data stack
- (NSManagedObjectContext *)defaultContext
{
    if (_defaultContext != nil) {
        return _defaultContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _defaultContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_defaultContext setPersistentStoreCoordinator:coordinator];
    }
    return _defaultContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:DMManagerModelNameValue withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self storeURL];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}  error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.defaultContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [[NSFileManager defaultManager] removeItemAtURL:[self storeURL] error:nil];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)clearContext
{
  
 
    _defaultContext = nil;
    _persistentStoreCoordinator = nil;
    _managedObjectModel = nil;
    
    NSError *error;
    NSURL *applicationDocumentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent:[DMManagerModelNameValue stringByAppendingString:@".sqlite"]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:storeURL.path isDirectory:NO]) {
        [[NSFileManager defaultManager] removeItemAtPath:storeURL.path error:&error];
    }
    
    
    if (error) {
        NSLog(@"%s error: %@", __FUNCTION__, [error localizedDescription]);
    }

}

#pragma mark - Serialization
+ (BOOL)serializeDataDictionary:(NSDictionary *)dictionary error:(NSError **)error
{
    
    return YES;

}

@end
